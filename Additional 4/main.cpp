#include <iostream>

/*Посчитайте количество всех возможных счастливых трамвайных билетиков. 
 Номер билета содержит 6 цифр.*/
int main()
{
	int count = 1;
	int arr[6];

	for (int i = 1000; i < 1000000; i++)
	{
		int tmp = i;
		for (int j = 0; j < 6; j++)
		{
			arr[j] = tmp % 10;
			tmp /= 10;
		}
		if (i < 1000000)
			if ((arr[0] + arr[1] + arr[2]) == (arr[3] + arr[4] + arr[5]))
				count += 1;
	}
	std::cout << count;
	return 0;
}
