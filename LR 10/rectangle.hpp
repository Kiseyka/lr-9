#pragma once
#include <SFML/Graphics.hpp>

namespace ek
{
	class Rectangle
	{
	public:
		Rectangle(int x, int y, int w, int h, float velocity);

		~Rectangle();

		void Move();

		void SetY(int y);

		int GetY();
		sf::RectangleShape* Get();
		int GetH();

	private:
		int m_x, m_y;
		int m_w, m_h;
		float m_velocity;
		sf::RectangleShape* m_shape;
	};
}