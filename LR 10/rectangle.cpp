#include <rectangle.hpp>

namespace ek
{
	Rectangle::Rectangle(int x, int y, int w, int h, float velocity)
	{
		m_w = w;
		m_h = h;
		m_x = x;
		m_y = y;
		m_velocity = velocity;

		m_shape = new sf::RectangleShape(sf::Vector2f(m_w, m_h));
		m_shape->setOrigin(m_w, m_h / 2);
		m_shape->setFillColor(sf::Color::Cyan);
		m_shape->setPosition(m_x, m_y);
	}
	Rectangle::~Rectangle()
	{
		delete m_shape;
	}
	sf::RectangleShape* Rectangle::Get() { return m_shape; }

	void Rectangle::Move()
	{
		m_y -= m_velocity;
		m_shape->setPosition(m_x, m_y);
	}

	void Rectangle::SetY(int y)
	{
		m_y = GetH() / 2;
		m_shape->setPosition(m_x, m_y);
	}

	int Rectangle::GetY() { return m_y; }
	int Rectangle::GetH() { return m_h; }
}