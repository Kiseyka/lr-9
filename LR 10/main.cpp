﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <rectangle.hpp>
#include <vector>

using namespace std::chrono_literals;

/*Создайте 3 прямоугольника разных цветов и размеров.
Перемещайте их из исходного положения вверх до конца
экрана с разной скорость.При достижении конца экрана остановите фигуры.*/

int main()
{
	srand(time(0));
	// ширина, высота окна; количество фигур
	const int width = 800;
	const int height = 600;
	const int N = 3;
	// Создание окна с известными размерами и названием
	sf::RenderWindow window(sf::VideoMode(width, height), L"Первая программа!");

	std::vector<ek::Rectangle*> rectangles;
	for (int i = 0; i <= width; i += (width / N))
		rectangles.push_back(new ek::Rectangle(i, 200, 100, 100, rand() % 5 + 1));
	// Цикл работает до тех пор, пока окно открыто
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		// Обработка
		for (const auto& rectangle : rectangles)
		{
			rectangle->Move();
			
			if (rectangle->GetY() < rectangle->GetH()/2)
				rectangle->SetY(0);
		}

		window.clear();

		for(const auto& rectangle :rectangles)
			window.draw(*rectangle->Get());
		// Отобразить на окне все, что есть в буфере
		window.display();

		std::this_thread::sleep_for(30ms);
	}

	for (const auto& rectangle : rectangles)
		delete rectangle;
	rectangles.clear();

	return 0;
}