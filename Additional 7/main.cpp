#include <iostream>
#include <vector>
#include <cmath>

//Дана последовательность натуральных чисел{ Aj }j = 1...n(n <= 10000).
//Удалить из последовательности числа, содержащие цифру 0, 
//но не содержащие цифру 1, а среди оставшихся продублировать числа, 
//являющиеся квадратом целого числа.

int Del(int& x) // Функция проверяет нужно ли удалять элемент
{
	int tmp = x;
	while (tmp > 0)
	{
		int d = tmp % 10;
		if (d == 1)
			return x;
		tmp /= 10;
	}
	tmp = x;
	while (tmp > 0)
	{
		int d = tmp % 10;
		if (d == 0)
		{
			x = -1;
			return x;
		}	
		tmp /= 10;
	}
	return x;
}

bool Insert(int& x) // Функция проверяет нужно ли дублировать элемент
{
	int num = sqrt(x);
	if (num * num == x)
		return 1;
	else
		return 0;
}

int main()
{
	int n;
	std::cin >> n;
	std::vector<int> arr;
	int a;
	for (int i = 0; i < n; i++)
	{
		std::cin >> a;
		arr.push_back(a); // Заполняем массив
	}
	std::cout << std::endl;

	for (int i = 0; i < n; i++)
		Del(arr[i]); 

	for (int i = 0; i < arr.size(); i++)
		if (arr[i] == -1) // Удаляем элементы
		{
			arr.erase(arr.begin() + i);
			i--;
		}

	for (int i = 0; i < arr.size(); i++)
		if (Insert(arr[i]) == 1) // Добавляем элементы
		{
			arr.insert(arr.begin() + i, arr[i]);
			i++;
		}	

	for (int i = 0; i < arr.size(); i++) // Выводим результат
		std::cout << arr[i] << std::endl;
}