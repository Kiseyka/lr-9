#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
//#include <bird.hpp>
#include <iostream>
//#include <pig.hpp>
#include <circle.hpp>

//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")


using namespace std::chrono_literals;

int main()
{
    float t = 0; // �������


	sf::RenderWindow window(sf::VideoMode(800, 600), "Angry Birds");

    float sx = 20;
    float sy = 580;

	ek::Circle circle(20, 580, 20, 75, 75);

    // ���� �������� �� ��� ���, ���� ���� �������
    while (window.isOpen())
    {
        // ���������� ��� �������� �������
        sf::Event event;
        // ���� �� ���� ��������
        while (window.pollEvent(event))
        {
            // ��������� �������
            // ���� ����� �������, ��
            if (event.type == sf::Event::Closed)
                // ���� �����������
                window.close();
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            //while (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {}
            sf::Vector2i mp = sf::Mouse::getPosition(window);
            std::cout << mp.x << " " << mp.y << std::endl;

            float d = sqrt((mp.x - sx) * (mp.x - sx) + (mp.y - sy) * (mp.y - sy));
            float dy = sy - mp.y;
            float dx = mp.x - sx;
            float angle = atan(dy / dx);
            std::cout << d << " " << angle << std::endl;
        }

        // �������� ������
        circle.Move(t);

        // ����� �� �����
        window.clear();

        // ����� ����
        window.draw(*circle.Get());

        // ���������� �� ���� ���, ��� ���� � ������
        window.display();

        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

}
