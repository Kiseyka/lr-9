﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

/*Создайте 3 прямоугольника разных цветов и размеров.
Перемещайте их из исходного положения вверх до конца
экрана с разной скорость.При достижении конца экрана остановите фигуры.*/

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), L"Первая программа!");

	// Бирюзовый прямоугольник
	sf::RectangleShape shape1(sf::Vector2f(200, 100));
	shape1.setFillColor(sf::Color::Cyan);
	shape1.setOrigin(100, 50);
	int shape1_x = 550, shape1_y = 550;
	shape1.setPosition(shape1_x, shape1_y);

	// Синий прямоугольник
	sf::RectangleShape shape2(sf::Vector2f(90, 60));
	shape2.setFillColor(sf::Color::Blue);
	shape2.setOrigin(45, 45);
	int shape2_x = 450, shape2_y = 450;
	shape2.setPosition(shape2_x, shape2_y);

	// Зелёный прямоугольник
	sf::RectangleShape shape3(sf::Vector2f(150, 120));
	shape3.setFillColor(sf::Color::Green);
	shape3.setOrigin(75, 60);
	int shape3_x = 300, shape3_y = 200;
	shape3.setPosition(shape3_x, shape3_y);


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		//Перемещение бирюзового прямоугольника
		shape1_y -= 4;
		if (shape1_y < 50)
		{
			shape1_y = 50;
			shape1_x = 550;
		}
		shape1.setPosition(shape1_x, shape1_y);

		//Перемещение синего прямоугольника
		shape2_x -= 3;
		shape2_y -= 3;
		if (shape2_y < 45)
		{
			shape2_y = 45;
			shape2_x = 45;
		}

		shape2.setPosition(shape2_x, shape2_y);
		//Перемещение зелёного прямоугольника
		shape3_y -= 2;
		if (shape3_y < 60)
			shape3_y = 60;
		shape3.setPosition(shape3_x, shape3_y);

		window.clear();
		window.draw(shape1);
		window.draw(shape2);
		window.draw(shape3);
		window.display();

		std::this_thread::sleep_for(30ms);
	}
	return 0;
}