#include <functions.hpp>
namespace ek
{
	void Swap(int& a, int& b)
	{
		int tmp = a;
		a = b;
		b = tmp;
	}

	int Minn(int n, int arr[N])
	{
		int minn = 1000000;
		for (int i = 0; i < n; i++)
			if (arr[i] < minn)
				minn = arr[i];
		return minn;
	}

	int Maxx(int n, int arr[N])
	{
		int maxx = -1000000;
		for (int i = 0; i < n; i++)
			if (arr[i] > maxx)
				maxx = arr[i];
		return maxx;
	}

	void Sort(int n, int arr[N])
	{
		for (int i = 0; i < n - 1; i++)
			for (int j = i + 1; j < n; j++)
				if (arr[i] > arr[j])
					Swap(arr[i], arr[j]);
	}

}