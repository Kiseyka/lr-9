#include <functions.hpp>
#include <iostream>

//Введите последовательность целых чисел длиной не более 1000 элементов.
//Найдите минимальный и максимальный элемент, 
//отсортируйте последовательность по возрастанию.

int main()
{
	// Добавляем русский
	setlocale(LC_ALL, "Rus");
	// Добавляем исходные данные
	int n;
	std::cin >> n;
	int arr[N];
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
	// Выводим минимальные и максимальные элементы
	std::cout <<"Минимальный элемент: "<< ek::Minn(n, arr) << std::endl;
	std::cout << "Максимальный элемент: " << ek::Maxx(n, arr) << std::endl;
	// Сортируем
	ek::Sort(n, arr);
	// Выводим отсортированный массив
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << std::endl;

	return 0;
}