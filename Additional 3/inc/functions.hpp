#pragma once
#define N 1000
namespace ek
{
	// Функция меняет местами два элемента
	void Swap(int& a, int& b);
	// Функция поиска минимального элемента
	int Minn(int n, int arr[N]);
	// Функция поиска максимального элемента
	int Maxx(int n, int arr[N]);
	//Функция сортировки
	void Sort(int n, int arr[N]);
}