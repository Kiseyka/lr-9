#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <board.hpp>
#include <fruits.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;

int main()
{
    const int width = 1024; // ������ ����
    const int height = 768; // ������ ����
    const int N = 50;
    int score = 0; // ����

    // �������� ���� � ���������� ��������� � ���������
    sf::RenderWindow window(sf::VideoMode(width, height), L"Game!");

    // ���
    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR: not found back.jpg" << std::endl;
        return -1;
    }
    sf::Sprite background(texture);

    // �����, �����
    sf::Font font;
    if (!font.loadFromFile("fonts/arial.ttf"))
    {
        std::cout << "ERROR: font was not loaded." << std::endl;
        return -1;
    }

    sf::Text text;
    text.setFont(font);
    text.setString("Hello world");
    text.setCharacterSize(36);
    text.setFillColor(sf::Color::Red);

    // ������
    sf::Image icon;
    if (!icon.loadFromFile("img/fruit32.png"))
    {
        return -1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());

    // ������ �����
    std::vector<ek::Fruit*> fruits;
    for (int i = 0; i < N; i += 1)
        fruits.push_back(new ek::Fruit(rand() % 500 + 300, i*(-800), 50,20));

   // ��������� �������� �����
    for (const auto& fruit : fruits)
        if (!fruit->Setup())
            return -1;

    ek::Basket* basket = nullptr;

    // ���� �������� �� ��� ���, ���� ���� �������
    while (window.isOpen())
    {
        // ���������� ��� �������� �������
        sf::Event event;
        // ���� �� ���� ��������
        while (window.pollEvent(event))
        {
            // ��������� �������
            // ���� ����� �������, ��
            if (event.type == sf::Event::Closed)
                // ���� �����������
                window.close();
        }
        // �������� ��������
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i mp = sf::Mouse::getPosition(window);
            float x1 = mp.x;

            if (basket != nullptr)
                delete basket;

            basket = new ek::Basket(mp.x, height-170, 100, 200);

            if (!basket->Setup())
            {
                delete basket;
                window.close();
                return -1;
            }
        }
        // �������� ��������
        if (basket != nullptr)
        {
            sf::Vector2i mp = sf::Mouse::getPosition(window);
            basket->Move(mp.x);
            for (int i = 0; i < fruits.size(); i++)
            {
                int Y = basket->GetY();
                int X = basket->GetX() + 120;
                int y = fruits[i]->GetY();
                int x = fruits[i]->GetX();

                if ((abs((Y - y)) < 15) && (abs((X - x)) < 60))
                {
                    delete fruits[i];
                    fruits.erase(fruits.begin() + i);
                    i--;
                    score++;
                }     
            }
        }

        // �������� ������
        for (const auto& fruit : fruits)
        {
            fruit->Move();
            if (fruit->GetY() > height)
            {
                fruit->SetVelocity(20);
                fruit->SetY(-N*800);
                fruit->SetX(rand() % 500 + 200);
            }
        }

        // �������� ���� �� �����
        window.clear();

        // ���
        window.draw(background);

        // ����
        text.setString(std::string("Click to start!") + std::string("\nScore ") + std::to_string(score));
        window.draw(text);

        // ����������� ����� � �����
        for (const auto& Fruit : fruits)
            window.draw(*Fruit->Get());

        // ����������� �������� � �����
        if (basket != nullptr)
            window.draw(*basket->Get());

        // ���������� �� ���� ���, ��� ���� � ������
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    for (const auto& Fruit : fruits)
        delete Fruit;
    fruits.clear();

    if (basket != nullptr)
        delete basket;

    return 0;
}
