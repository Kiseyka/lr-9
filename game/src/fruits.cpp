#include <fruits.hpp>

namespace ek
{
	Fruit::Fruit(int x, int y, float r, float velocity)
	{
		m_x = x;
		m_y = y;
		m_r = r;
		m_velocity = velocity;
	}
	bool Fruit::Setup()
	{
		if (!m_texture.loadFromFile("img/fruit.png"))
		{
			std::cout << "ERROR when loading fruit.png" << std::endl;
			return false;
		}

		m_fruit = new sf::Sprite();
		m_fruit->setTexture(m_texture);
		m_fruit->setOrigin(m_r, m_r);
		m_fruit->setPosition(m_x, m_y);
		m_fruit->setScale(0.1, 0.1);

		return true;
	}

	Fruit::~Fruit()
	{
		if (m_fruit != nullptr)
			delete m_fruit;
	}


	sf::Sprite* Fruit::Get() { return m_fruit; }

	void Fruit::Move()
	{
		m_y += m_velocity;
		m_fruit->setPosition(m_x, m_y);
	}

	void Fruit::SetY(int y)
	{
		m_y = y;
		m_fruit->setPosition(m_x, m_y);
	}
	void Fruit::SetX(int x)
	{
		m_x = x;
		m_fruit->setPosition(m_x, m_y);
	}

	int Fruit::GetY() { return m_y; }
	int Fruit::GetX() { return m_x; }

	void Fruit::SetVelocity(int velocity)
	{
		m_velocity = velocity;
	}
}
