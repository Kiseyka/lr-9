#include <board.hpp>

namespace ek
{
	Basket::Basket(int x, int y, int w, int h)
	{
		m_w = w;
		m_h = h;
		m_x = x;
		m_y = y;
	}
	bool Basket::Setup()
	{
		if (!m_texture.loadFromFile("img/basket.png"))
		{
			std::cout << "ERROR when loading basket.png" << std::endl;
			return false;
		}

		m_basket = new sf::Sprite();
		m_basket->setTexture(m_texture);
		m_basket->setOrigin(m_w / 2, m_h / 2);
		m_basket->setPosition(m_x, m_y);
		m_basket->setScale(0.3, 0.3);

		return true;
	}

	Basket::~Basket()
	{
		if (m_basket != nullptr)
			delete m_basket;
	}

	int Basket::GetY() { return m_y; }
	int Basket::GetX() { return m_x; }

	void Basket::Move(int x)
	{
		m_x = x;
		m_basket->setPosition(m_x, m_y);
	}
	sf::Sprite* Basket::Get() { return m_basket; }
}
