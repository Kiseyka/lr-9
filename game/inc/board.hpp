#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

namespace ek
{
	class Basket
	{
	public:
		Basket(int x, int y, int w, int h);

		~Basket();

		bool Setup();	

		void Move(int x);

		sf::Sprite* Get();;

		int GetY();
		int GetX();

	private:
		int m_x, m_y;
		int m_w, m_h;

		sf::Texture m_texture;
		sf::Sprite* m_basket = nullptr;

	};
}
