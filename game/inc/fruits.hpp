#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

namespace ek
{
	class Fruit
	{
	public:
		Fruit(int x, int y, float r, float velocity);

		~Fruit();

		bool Setup();
		sf::Sprite* Get();

		void Move();

		void SetY(int y);
		void SetX(int x);
		void SetVelocity(int velocity);

		int GetY();
		int GetX();

	private:
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::Texture m_texture;
		sf::Sprite* m_fruit = nullptr;

	};

}
