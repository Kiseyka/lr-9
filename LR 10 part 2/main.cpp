#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <circles.hpp>
#include <vector>

/*Создайте N(0 < N < 100) кругов.
Перемещайте их из исходного положения вверх до конца экрана
с разной скоростью. При достижении конца экрана остановите фигуры.*/

using namespace std::chrono_literals;

int main()
{
	srand(time(0));
    setlocale(LC_ALL, "Rus");

    const int width = 800;
    const int height = 600;
    int N;
    std::cout << "Введите количество фигур(от 0 до 100): " << std::endl;
    std::cin >> N;

    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(width, height), L"ЛР№10 Вариант 8!");

    std::vector<ek::Circle*> circles;
    for (int i = 0; i <= width; i += width / N)
        circles.push_back(new ek::Circle(i, rand()%599 + 2, 15, rand() % 5 + 1));

    while (window.isOpen())
    {
       // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }

        // Цикл движения
        for (const auto& circle : circles)
        {
            circle->Move();
            if (circle->GetY() < circle->GetR())
            {
                circle->SetY(circle->GetR());
            }
        }
        // Очистить окно от всего
        window.clear();

        // Перемещение фигуры в буфер
        for (const auto& circle : circles)
            window.draw(*circle->Get());

        // Отобразить на окне все, что есть в буфере
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    for (const auto& circle : circles)
        delete circle;
    circles.clear();

    return 0;
}

