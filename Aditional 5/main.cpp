#include <iostream>
#include <cmath>

#define N 1000

// Условие задачи
/*Дана последовательность натуральных чисел{ Aj }j = 1...n(n <= 10000).
Удалить из последовательности числа, содержащие цифру 0,
но не содержащие цифру 1, а среди оставшихся продублировать числа,
являющиеся квадратом целого числа.*/

// Функция присваивает числам содержащим цифру 0 и не содержащим цифру 1 значение = -1
int Del(int& x)
{
	int num1 = x;
	while (num1 > 0) // Цикл проверяет есть ли в числе цифра 1
	{
		int last_num = num1 % 10;
		if (last_num == 1)
			return x; // если находит, то сразу возвращает это число
		num1 /= 10; // если не находит, то идём дальше
	}
	int num2 = x;
	while (num2 > 0) // цикл проверяет есть ли цифра 0 в числе
	{
		int last_num = num2 % 10;
		if (last_num == 0)
		{
			x = -1;
			return x; // если находит, то приравнивает х = -1 и возвращает
		}
		num2 /= 10;
	}
	return x; // если в числе нет ни 1, ни 0, то просто возвращает х
}

//Функция считает количество элментов < 0
int Counter_M(int arr[N], int n, int& kol)
{
	for (int i = 0; i < n; i++)
		if (arr[i] < 0)
			kol += 1;
	return kol; 
}

// Функция считает количество чисел, которые являются квадратами целых чисел
int Counter_C(int arr[N], int n, int& kol_db)
{
	for (int i = 0; i < n; i++)
	{
		int a = sqrt(arr[i]);
		if ((a * a) == arr[i])
			kol_db += 1;
	}
	return kol_db;
}

// Процедура сортировки массива
void Sort(int n, int arr[N])// Cортирует массив таким образом,
{		// Что все элементы < 0 переносятся в конец
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (arr[i] < 0)
				if (arr[j] > 0)
					std::swap(arr[i], arr[j]);
}
// Процедура сдвига массива
void Shift(int n, int arr[N], int ind, int kol, int kol_db, int t) // Cдвигает элементы вправо
{							// Как бы освобождая ячейку массива
	for (int i = (n + t); i > (ind + 1); i--)
		std::swap(arr[i], arr[i - 1]);
}

// Процедура ищет элементы, которые являются квадратами целых чисел
void DoubleNum(int n, int arr[N], int kol, int kol_db, int ind, int t)
{
	int j = 0;
	while (j != kol_db)
	{
		for (int i = 0; i < (n + t); i++)
		{
			int a = sqrt(arr[i]); // Если число - квадрат, то она вызывает
			if ((a * a) == arr[i]) // процедуру Shift и та сдвигает массив
			{			// от этого элемента вправо. Далее процедура
				ind = i; // дублирует в "освободившееся место" своё значение
				Shift(n, arr, ind, kol, kol_db, t);
				arr[i + 1] = arr[i];
				j++;
				i++;
			}
		}
	}
}

int main()
{
	int n; // количество элементов последовательности
	std::cin >> n;
	int arr[N];  // массив
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
	std::cout << std::endl;

	int ind = 0; // переменная хранит индекс элемента, от которого будет сдвигаться массив

	int kol = 0; // переменная хранит количество отрицательных элементов
	int kol_db = 0; // переменная хранит количество элементов квадратов

	for (int i = 0; i < n; i++) // Отмечаем лишние элементы
		Del(arr[i]);

	Counter_M(arr, n, kol); // Считаем значение kol - элементы < 0
	Counter_C(arr, n, kol_db); // Считаем значение kol_db - элементы "квадраты"

	Sort(n, arr); // Сортируем массив, лишние элементы убираем в конец

	int t = kol_db - kol;
	DoubleNum(n, arr, kol, kol_db, ind, t); // Дублируем квадраты

	for (int i = 0; i < (n + t); i++)
		std::cout << arr[i] << std::endl; // Выводим результат

	return 0;
}